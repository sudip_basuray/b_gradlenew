package b_gradlenew;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Flightfindersuite 
{

	WebDriver Driver;
	Method_repo mr =new Method_repo();
	@BeforeMethod
	public void applaunch() throws InterruptedException
	{
		mr.browserAppLaunch();
	}
	
	@Test(priority=0)
	public void verifyDefaultSelectionFlightType()
	{
		try
		{
			mr.login("dasd", "dasd");
			Assert.assertEquals(true, mr.verifyDefaultSelectionRoundTrip());
		}
		catch(Exception e){
			System.out.println(e);
		}
	}
	@Test(priority=1)
	public void verifyDepartingFromValueSelection() throws InterruptedException
	{
		try {
		mr.login("dasd", "dasd");
		Assert.assertEquals(false, mr.departingFromValueSelection());
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
	@AfterMethod
	public void appclose()
	{
		mr.appClose();
	}
}
