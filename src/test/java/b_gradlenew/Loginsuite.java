package b_gradlenew;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Loginsuite 
{

	WebDriver driver;
	Method_repo mr1 =new Method_repo();
	@BeforeMethod
	public void launch() throws InterruptedException
	{
		mr1.browserAppLaunch();
	}
	@Test(priority=0)
	public void veryfylogin() throws InterruptedException
	{
		try {
		mr1.login("dasd", "dasd");
		Assert.assertEquals(true, mr1.verifyInValidLogin());
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	@Test(priority=1)
	public void  invalidlogin()
	{
		try {
			mr1.login("dasds", "dasds");
			Assert.assertEquals(true, mr1.verifyInValidLogin());
		}
		catch(Exception e)
		{
			System.out.println(e);
		
		}
	}
	@AfterMethod
	public void closebrowser()
	{
		mr1.appClose();
	}
	
}
